package hu.robert.kocsis.template.presentation.home

import androidx.lifecycle.MutableLiveData
import hu.robert.kocsis.template.data.model.Country
import hu.robert.kocsis.template.domain.home.repository.EuCountriesRepository
import hu.robert.kocsis.template.presentation.core.BaseViewModel

class HomeViewModel constructor(private val euCountriesRepository: EuCountriesRepository) : BaseViewModel() {
    init {
        loadCountries()
    }

    var data: MutableLiveData<List<Country>> = MutableLiveData()

    private fun loadCountries() = addDisposable(euCountriesRepository.getCountryList()
            // TODO get list when data is empty, or forced to get
            .subscribe {
                it.either(::handleFailure, ::handleSuccess)
            })

    private fun handleSuccess(datas: List<Country>) {
        this.data.value = datas
    }

}