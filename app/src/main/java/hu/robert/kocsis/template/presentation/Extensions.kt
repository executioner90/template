package hu.robert.kocsis.template.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import hu.robert.kocsis.template.App
import hu.robert.kocsis.template.data.exception.Failure
import hu.robert.kocsis.template.data.functional.Either
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import org.koin.android.viewmodel.ext.android.getViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

val Activity.app: App
    get() = application as App

fun <T> Call<T>.getResult(): LiveData<Either<Failure, T>> {
    val data = MutableLiveData<Either<Failure, T>>()
    this.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            response.isSuccessful.let {
                data.value = Either.Right(response.body()!!)
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            data.value = Either.Left(Failure.ServerError())
        }
    })
    return data
}

inline fun <reified T : ViewModel> FragmentActivity.withViewModel(body: T.() -> Unit): T {
    val vm = getViewModel<T>()
    vm.body()
    return vm
}

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T?) -> Unit) =
        liveData.observe(this, Observer(body))

fun <L : LiveData<Failure>> LifecycleOwner.failure(liveData: L, body: (Failure?) -> Unit) =
        liveData.observe(this, Observer(body))

@SuppressLint("CheckResult")
fun <T> Single<T>.getResult(scheduler: Scheduler): Observable<Either<Failure, T>> {
    val data = BehaviorSubject.create<Either<Failure, T>>()
    this.subscribeOn(scheduler)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                    { t ->
                        data.onNext(Either.Right(t))
                    },
                    { error ->
                        data.onNext(Either.Left(Failure.ServerError()))
                        Log.e("Result", "Unable to get results", error)
                    }
            )
    return data
}
