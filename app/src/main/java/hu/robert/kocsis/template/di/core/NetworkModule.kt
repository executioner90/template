package hu.robert.kocsis.template.di.core

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import hu.robert.kocsis.template.di.RETROFIT_BASE
import hu.robert.kocsis.template.di.core.NetworkProperties.BASE_URL
import okhttp3.OkHttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object NetworkProperties {
    const val BASE_URL = "https://restcountries.eu/rest/v2/region/"
}

val networkModule = module {
    single { httpClient() }
    single<Moshi> {
        Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()
    }
    single(named(RETROFIT_BASE)) { createRetrofit(getProperty(BASE_URL)) }
}

fun createRetrofit(baseUrl: String, httpClient: OkHttpClient = httpClient()) =
        Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

private fun httpClient(): OkHttpClient {
    val clientBuilder = OkHttpClient.Builder()
    return clientBuilder.build()
}

inline fun <reified T> retrofitClient(baseUrl: String = BASE_URL): T {
    val retrofit = createRetrofit(baseUrl)
    return retrofit.create(T::class.java)
}