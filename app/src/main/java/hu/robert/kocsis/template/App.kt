package hu.robert.kocsis.template

import android.app.Application
import hu.robert.kocsis.template.di.core.applicationModule
import hu.robert.kocsis.template.di.core.networkModule
import hu.robert.kocsis.template.di.home.homeModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(applicationModule,
                    networkModule,
                    homeModule)
        }
    }
}