package hu.robert.kocsis.template.di.home

import hu.robert.kocsis.template.data.service.EUCountryService
import hu.robert.kocsis.template.di.SCHEDULERS_IO
import hu.robert.kocsis.template.di.core.retrofitClient
import hu.robert.kocsis.template.domain.home.repository.EuCountriesRepository
import hu.robert.kocsis.template.presentation.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module


val homeModule = module {
    single { retrofitClient<EUCountryService>() }
    single { EuCountriesRepository(get(), get(named(SCHEDULERS_IO))) }

    viewModel {
        HomeViewModel(get())
    }
}

