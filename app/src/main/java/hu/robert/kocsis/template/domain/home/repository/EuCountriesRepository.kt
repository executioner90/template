package hu.robert.kocsis.template.domain.home.repository

import hu.robert.kocsis.template.data.exception.Failure
import hu.robert.kocsis.template.data.functional.Either
import hu.robert.kocsis.template.data.model.Country
import hu.robert.kocsis.template.data.service.EUCountryService
import hu.robert.kocsis.template.presentation.getResult
import io.reactivex.Observable
import io.reactivex.Scheduler


class EuCountriesRepository(private val euCountryService: EUCountryService,
                            private val scheduler: Scheduler) {

    fun getCountryList(): Observable<Either<Failure, List<Country>>> {
        return euCountryService.countriesList.getResult(scheduler)
    }
}
