package hu.robert.kocsis.template.presentation.home

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import hu.robert.kocsis.template.R
import hu.robert.kocsis.template.data.exception.Failure
import hu.robert.kocsis.template.data.model.Country
import hu.robert.kocsis.template.presentation.failure
import hu.robert.kocsis.template.presentation.observe
import hu.robert.kocsis.template.presentation.withViewModel


class HomeActivity : AppCompatActivity() {
    // UI
    lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById(R.id.textView)

        withViewModel<HomeViewModel> {
            observe(data, ::renderCountries)
            failure(fail, ::handleFailure)
        }
    }

    private fun renderCountries(countries: List<Country>?) {
        textView.text = "Talalt orszagok szama: $countries.orEmpty().size"
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.ServerError -> textView.text = "Hiba a matrixban"
        }
    }
}