package hu.robert.kocsis.template.data.model

data class Country(val name: String)