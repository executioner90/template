package hu.robert.kocsis.template.di.core

import hu.robert.kocsis.template.di.SCHEDULERS_IO
import hu.robert.kocsis.template.di.SCHEDULERS_MAIN
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.qualifier.named
import org.koin.dsl.module

val applicationModule = module {
    single(named(SCHEDULERS_IO)) { Schedulers.io() }
    single(named(SCHEDULERS_MAIN)) { AndroidSchedulers.mainThread() }
}