package hu.robert.kocsis.template.data.service;

import java.util.List;

import hu.robert.kocsis.template.data.model.Country;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface EUCountryService {

    @GET("europe")
    Single<List<Country>> getCountriesList();
}
